import httpx
from PIL import Image
from io import BytesIO
import pytesseract
from typing import Any

async def image_url(image_url: str) -> str:
    """
    Extracts text from an image using pytesseract.

    Args:
        image_url (str): URL of the image to extract text from.

    Returns:
        str: Extracted text from the image.
    """
    async with httpx.AsyncClient() as client:
        response = await client.get(image_url)
        response.raise_for_status()
        img = Image.open(BytesIO(response.content))
        text = pytesseract.image_to_string(img)
        return text


async def image_file(file: Any) -> str:
    """
    Extracts text from an uploaded image using pytesseract.

    Args:
        file (Any): The uploaded image file.

    Returns:
        str: Extracted text from the image.
    """
    contents = await file.read()
    img = Image.open(BytesIO(contents))
    text = pytesseract.image_to_string(img)
    return text
