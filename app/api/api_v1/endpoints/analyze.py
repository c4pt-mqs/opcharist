from fastapi import FastAPI, Query, UploadFile, File, HTTPException
from aiocache import Cache
from schemas.schemas import SensitiveDataResponse
from services.ocr_analyze import image_url, image_file
from utils.data_analysis import analyze_data

app = FastAPI()

cache = Cache(Cache.MEMORY)

@app.get("/")
async def root_dir():
    return {"message": "Server is running!"}

async def image_common(extracted_text):
    extracted_text = extracted_text.replace("\n", " ")
    sensitive_data = await analyze_data(extracted_text)
    return SensitiveDataResponse(
        content=extracted_text,
        status="successful",
        findings=sensitive_data
    )

@app.post("/analyze/")
async def analyze_image(url: str = Query(..., description="URL of the image")):
    cached_result = await cache.get(url)
    if cached_result:
        return cached_result

    extracted_text = await image_url(url)
    response = await image_common(extracted_text)

    await cache.set(url, response, ttl=3600)

    if not response.content:
        raise HTTPException(status_code=204, detail="No content")

    if "bad request" in response.status.lower():
        raise HTTPException(status_code=400, detail="Bad request. Wrong file format.")

    return response

@app.post("/upload/")
async def upload_and_analyze(file: UploadFile = File(...)):
    cached_result = await cache.get(file.filename)
    if cached_result:
        return cached_result

    extracted_text = await image_file(file)
    response = await image_common(extracted_text)

    await cache.set(file.filename, response, ttl=3600)

    if not response.content:
        raise HTTPException(status_code=204, detail="No content")

    if "bad request" in response.status.lower():
        raise HTTPException(status_code=400, detail="Bad request. Wrong file format.")

    return response
