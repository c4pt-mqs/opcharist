from pydantic import BaseModel
from typing import List
from enums import SensitiveDataType

class SensitiveData(BaseModel):
    value: str
    type: SensitiveDataType
    valid: bool

class SensitiveDataResponse(BaseModel):
    content: str
    status: str
    findings: List[SensitiveData]
    