from dynaconf import Dynaconf

settings = Dynaconf(
    load_dotenv=True,
    dotenv_path="../env/.env",
    envvar_prefix_for_dynaconf=False,
)
