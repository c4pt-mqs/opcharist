import uvicorn
from api.api_v1.endpoints.analyze import app
from core.config import settings

if __name__ == "__main__":
    uvicorn.run(app, host=settings.api_host, port=settings.api_port)

## uvicorn main:app --reload