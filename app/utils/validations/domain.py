import whois

def validate_domain(domain: str) -> bool:
    """
    Validates a domain using the WHOIS lookup to check its availability.

    Args:
        domain (str): The domain to be validated.

    Returns:
        bool: True if the domain is valid and has WHOIS records, False otherwise.
    """
    try:
        w = whois.whois(domain)
        if w.status:
            return True
        else:
            return False
    except Exception as e:
        return False
