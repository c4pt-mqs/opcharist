import re

def validate_combolist(combolist: str) -> bool:
    """
    Validates a combolist to ensure each line is in the format "username:password".

    Args:
        combolist (str): The combolist text to be validated.

    Returns:
        bool: True if all lines are in the valid format, False otherwise.
    """
    lines = combolist.strip().split("\n")

    for line in lines:
        if not re.match(r"^[^:\s]+:[^\s]+$", line):
            return False
    
    return True
