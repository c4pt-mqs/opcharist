import hashlib

def validate_hash(hash_string: str) -> bool:
    """
    Validates a hash string using common cryptographic hash algorithms.

    Args:
        hash_string (str): The hash string to be validated.

    Returns:
        bool: True if the hash string is valid, False otherwise.
    """
    valid_algorithms = ["md5", "sha1", "sha256"]
    
    for algorithm in valid_algorithms:
        try:
            hash_object = hashlib.new(algorithm, bytes.fromhex(hash_string))
            hash_object.digest()
            return True
        except (ValueError, TypeError, hashlib.HashDigestError):
            continue
    
    return False
