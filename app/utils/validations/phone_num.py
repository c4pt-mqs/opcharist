import phonenumbers

def validate_phone_number(phone_number: str, default_region: str = "US") -> bool:
    """
    Validates a phone number using the phonenumbers library.

    Args:
        phone_number (str): The phone number to be validated.
        default_region (str, optional): The default region for parsing the phone number. Defaults to "US".

    Returns:
        bool: True if the phone number is valid, False otherwise.
    """
    try:
        parsed_number = phonenumbers.parse(phone_number, default_region)
        if phonenumbers.is_possible_number(parsed_number) and phonenumbers.is_valid_number(parsed_number):
            return True
        return False
    except phonenumbers.NumberParseException:
        return False
