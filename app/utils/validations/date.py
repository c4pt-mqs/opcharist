import datetime

def validate_date(date: str) -> bool:
    """
    Validates a date string in the format 'MM/DD/YYYY'.

    Args:
        date (str): The date string to be validated.

    Returns:
        bool: True if the date string is valid, False otherwise.
    """
    try:
        datetime.datetime.strptime(date, '%m/%d/%Y')
        return True
    except ValueError:
        return False
