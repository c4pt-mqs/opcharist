def validate_credit_card_number(credit_card_number: str) -> bool:
    """
    Validates a credit card number using the Luhn algorithm.
    
    Args:
        credit_card_number (str): The credit card number to be validated.
        
    Returns:
        bool: True if the credit card number is valid, False otherwise.
    """
    nDigits = len(credit_card_number)
    nSum = 0
    isSecond = False
     
    for i in range(nDigits - 1, -1, -1):
        d = int(credit_card_number[i])
     
        if isSecond:
            d = d * 2
            d = d // 10 + d % 10
  
        nSum += d
        isSecond = not isSecond
     
    return nSum % 10 == 0
