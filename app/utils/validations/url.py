from urllib.parse import urlparse

def validate_url(url: str) -> bool:
    """
    Validates a URL format.

    Args:
        url (str): The URL to be validated.

    Returns:
        bool: True if the URL is valid, False otherwise.
    """
    try:
        result = urlparse(url)
        if result.scheme in ('http', 'https') and result.netloc:
            if url.startswith('http://') or url.startswith('https://'):
                domain_parts = result.netloc.split('.')
                if len(domain_parts) > 1:
                    subdomain, domain, *tld = domain_parts
                    if 1 <= len(subdomain) <= 63 and 2 <= len(domain) <= 63 and 0 <= sum(map(len, tld)) <= 63:
                        return True
            else:
                return False
        return False
    except ValueError:
        return False
