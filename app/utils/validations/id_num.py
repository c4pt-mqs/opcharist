def validate_id_number(id_number: str) -> bool:
    """
    Validates an ID number.

    Args:
        id_number (str): The ID number to be validated.

    Returns:
        bool: True if the ID number is valid, False otherwise.
    """
    return len(id_number) >= 9 and id_number.isdigit()
