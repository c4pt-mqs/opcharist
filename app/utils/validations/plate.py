def validate_plate(number_plate: str) -> bool:
    """
    Validates a number plate format.

    Args:
        number_plate (str): The number plate to be validated.

    Returns:
        bool: True if the number plate is valid, False otherwise.
    """
    if len(number_plate) != 7:
        return False

    if (number_plate[0:2].isalpha() and number_plate[2:4].isdigit() and number_plate[4:].isalpha()):
        return True

    return False
