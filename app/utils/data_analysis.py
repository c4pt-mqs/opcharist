from typing import List
from schemas.schemas import SensitiveData, SensitiveDataType
from utils.validations.phone_num import validate_phone_number
from utils.validations.id_num import validate_id_number
from utils.validations.credit_card_num import validate_credit_card_number
from utils.validations.plate import validate_plate
from utils.validations.date import validate_date
from utils.validations.email import validate_email
from utils.validations.domain import validate_domain
from utils.validations.url import validate_url
from utils.validations.hash import validate_hash
from utils.validations.combolist import validate_combolist
import regex as re

async def analyze_data(text: str) -> List[SensitiveData]:
    """
    Analyzes the given text for sensitive data using regular expressions.

    Args:
        text (str): The text to be analyzed.

    Returns:
        List[SensitiveData]: A list of sensitive data findings.
    """
    sensitive_data_findings = []
    
    regex_patterns = {
        SensitiveDataType.phone_number: r"\+\d{1,3} \(\d{3}\) \d{3}-\d{4}|\d{10,}",
        SensitiveDataType.id_number: r"\d{9,}",
        SensitiveDataType.credit_card_number: r"\b(?:\d{4}[ -]?){3}(?=\d{4}\b)(?:\d{4})",
        SensitiveDataType.plate: r"[A-Z]{1,3}-[A-Z]{1,2}-[0-9]{1,4}",
        SensitiveDataType.date: r"\d{1,2}/\d{1,2}/\d{2,4}",
        SensitiveDataType.email: r"[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
        SensitiveDataType.domain: r"(?!https?://)(?!-)(?![0-9]*\.)[a-z0-9\-\.]{1,63}(?<!-)(?<!\.)\.[a-z]{2,63}",
        SensitiveDataType.url: r"https?://(?:[\w-]{1,63}\.){1,127}[a-z]{2,63}",
        SensitiveDataType.hash: r"\b(?:[a-fA-F0-9]{32}|[a-fA-F0-9]{40}|[a-fA-F0-9]{64})\b",
        SensitiveDataType.combolist: r"(?:(?:\S+:\S+)\s*)+",
    }

    validation_functions = {
        SensitiveDataType.phone_number: validate_phone_number,
        SensitiveDataType.id_number: validate_id_number,
        SensitiveDataType.credit_card_number: validate_credit_card_number,
        SensitiveDataType.plate: validate_plate,
        SensitiveDataType.date: validate_date,
        SensitiveDataType.email: validate_email,
        SensitiveDataType.domain: validate_domain,
        SensitiveDataType.url: validate_url,
        SensitiveDataType.hash: validate_hash,
        SensitiveDataType.combolist: validate_combolist,
    }

    for data_type, pattern in regex_patterns.items():
        matches = re.findall(pattern, text)
        validation_function = validation_functions.get(data_type)

        for match in matches:
            if validation_function:
                is_valid = validation_function(match)
                sensitive_data_findings.append(SensitiveData(value=match, type=data_type, valid=is_valid))
            else:
                sensitive_data_findings.append(SensitiveData(value=match, type=data_type))

    return sensitive_data_findings
