from enum import Enum

class SensitiveDataType(str, Enum):
    phone_number = "phone_number"
    id_number = "id_number"
    credit_card_number = "credit_card_number"
    plate = "plate"
    date = "date"
    email = "email"
    domain = "domain"
    url = "url"
    hash = "hash"
    combolist = "combolist"